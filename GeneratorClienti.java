import java.util.Random;

public class GeneratorClienti {

    private int timpSosireMinim, timpSosireMaxim, timpServireMinim, timpServireMaxim, timpSimulare, viteza;

    public GeneratorClienti(int timpSosireMinim, int timpSosireMaxim, int timpServireMinim, int timpServireMaxim, int viteza) {
        this.timpSosireMinim = timpSosireMinim;
        this.timpSosireMaxim = timpSosireMaxim;
        this.timpServireMinim = timpServireMinim;
        this.timpServireMaxim = timpServireMaxim;
        this.viteza = viteza;
    }

    public int timpSosire(){
        Random rand=new Random();
        return  rand.nextInt(timpSosireMaxim-timpSosireMinim + 1)+timpSosireMinim;
    }

    public int timpServire(){
        Random rand=new Random();
        return rand.nextInt(timpServireMaxim-timpServireMinim+1)+timpServireMinim;
    }

    public int getTimpSimulare() {
        return timpSimulare;
    }

    public void setTimpSimulare(int timpSimulare) {
        this.timpSimulare = timpSimulare;
    }
}
