import javax.swing.*;
import java.util.*;

import static java.lang.Thread.sleep;

public class CasaMarcat implements Runnable {

    private Queue<Client> list;
    private int id;
    private int timpSimulare;
    private int timpEmpty;
    private int timpAsteptare;
    private int viteza;
    private JTextArea ta;

    public CasaMarcat(int id, int viteza, JTextArea ta) {
        list = new ArrayDeque();
        this.id = id;
        this.timpEmpty = 0;
        this.viteza = viteza;
        this.ta=ta;
        this.timpAsteptare=0;
    }

    public synchronized void addClient(Client c) {
        notify();
        list.add(c);
        ta.append("Add client id :" + c.getNumar() + " la casa :" + id + " la secunda: " + timpSimulare + "\n");

    }

    public Queue<Client> getList() {
        return list;
    }

    public int empty() {
        return list.size();
    }

    public int getTimpAsteptare() {
        return timpAsteptare;
    }

    public void setTimpAsteptare(int timpAsteptare) {
        this.timpAsteptare = timpAsteptare;
    }

    public int getTimpSimulare() {
        return timpSimulare;
    }

    public synchronized void setTimpSimulare(int timpSimulare) {
        this.timpSimulare = timpSimulare;
    }

    public int getTimpEmpty() {
        return timpEmpty;
    }

    public void setTimpEmpty(int timpEmpty) {
        this.timpEmpty = timpEmpty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        while (true) {
            if (empty() != 0) {
                ta.append("La casa cu id: " + id + " se serveste clietul cu id: " + list.element().getNumar() + " la secunda :" + timpSimulare+"\n");
                try {
                    sleep(viteza * 10 * list.element().getServingTime());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ta.append("La casa cu id: " + id + " a plecat clietul cu id: " + list.element().getNumar() + " la secunda :" + timpSimulare+"\n");
                list.remove();
            }
            synchronized (this) {
                if (empty() == 0) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }
}
