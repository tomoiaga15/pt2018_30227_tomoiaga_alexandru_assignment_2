import java.awt.*;
import javax.swing.*;

class Start extends JFrame {


    JPanel panel1, panel2, panel3, panel4, panel5, panel6, panel7;
    JLabel l1, l2, l3, l4 , l5, l6, l7;
    JTextField tf1, tf2, tf3, tf4, tf5, tf6, tf7;
    JButton b1;
    JTextArea ta, ta1;

    public Start(String nume) {
        setTitle(nume);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(480, 540);

        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();
        panel4 = new JPanel();
        panel5 = new JPanel();
        panel6 = new JPanel();
        panel7 = new JPanel();

        l1 = new JLabel("Min arriving time:");
        l2 = new JLabel("Max arriving time:");
        l3 = new JLabel("Min service time:");
        l4 = new JLabel("Max service time:");
        l5 = new JLabel("Number of queues:");
        l6 = new JLabel("Simulation interval:");
        l7 = new JLabel("Speed:");

        tf1 = new JTextField("-");
        tf2 = new JTextField("-");
        tf3 = new JTextField("-");
        tf4 = new JTextField("-");
        tf5 = new JTextField("-");
        tf6 = new JTextField("-");
        tf7 = new JTextField("-");
        ta1 = new JTextArea(10,20);
        ta = new JTextArea(10, 30);

        tf1.setColumns(7);
        tf2.setColumns(7);
        tf3.setColumns(7);
        tf4.setColumns(7);
        tf5.setColumns(7);
        tf6.setColumns(7);
        tf7.setColumns(7);

        JScrollPane scroll = new JScrollPane ( ta );
        scroll.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
        JScrollPane scrolll = new JScrollPane ( ta1 );
        scroll.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );

        panel1.setLayout(new FlowLayout());
        panel2.setLayout(new FlowLayout());
        panel3.setLayout(new FlowLayout());
        panel4.setLayout(new FlowLayout());
        panel5.setLayout(new FlowLayout());
        panel6.setLayout(new FlowLayout());
        panel7.setLayout(new FlowLayout());

        panel1.add(l1);
        panel1.add(tf1);
        panel1.add(l2);
        panel1.add(tf2);
        panel2.add(l3);
        panel2.add(tf3);
        panel2.add(l4);
        panel2.add(tf4);
        panel3.add(l5);
        panel3.add(tf5);
        panel3.add(l6);
        panel3.add(tf6);
        panel4.add(l7);
        panel4.add(tf7);

        b1 = new JButton("Start");
        panel5.add(b1);
        panel6.add(scroll);
        panel7.add(scrolll);

        b1.addActionListener(new Butoane(tf1, tf2, tf3, tf4, tf5, tf6, tf7, ta, ta1));

        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.add(panel3);
        p.add(panel4);
        p.add(panel5);
        p.add(panel6);
        p.add(panel7);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        setContentPane(p);
        setVisible(true);
    }

    public static void main(String args[]) {
        new Start("Pagina principala");
    }

}