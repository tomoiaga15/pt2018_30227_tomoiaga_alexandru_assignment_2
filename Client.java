public class Client {

    private int servingTime, arrivingTime;
    private int numar;

    public Client(int numar, int servingTime, int arrivingTime) {
        this.servingTime = servingTime;
        this.numar = numar;
        this.arrivingTime = arrivingTime;
    }

    public int getServingTime() {
        return servingTime;
    }

    public int getNumar() {
        return numar;
    }

    public void setServingTime(int servingTime) {
        this.servingTime = servingTime;
    }

    public void setNumar(int numar) {
        this.numar = numar;
    }

    public int getArrivingTime() {
        return arrivingTime;
    }

    public void setArrivingTime(int arrivingTime) {
        this.arrivingTime = arrivingTime;
    }
    @Override
    public String toString() {
        return "Client{" +
                "servingTime=" + servingTime +
                ", arrivingTime=" + arrivingTime +
                ", numar=" + numar +
                '}';
    }
}
