import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Butoane implements ActionListener {
    private JTextField t1,t2,t3,t4,t5,t6,t7;
    private JTextArea ta, ta1;
    public Butoane(JTextField t1, JTextField t2, JTextField t3, JTextField t4, JTextField t5, JTextField t6, JTextField t7, JTextArea ta, JTextArea ta1) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.t4 = t4;
        this.t5 = t5;
        this.t6 = t6;
        this.t7 = t7;
        this.ta = ta;
        this.ta1=ta1;
    }
    public void actionPerformed(ActionEvent event) {
        Magazin m=new Magazin(Integer.parseInt(t5.getText()), Integer.parseInt(t3.getText()), Integer.parseInt(t4.getText()), Integer.parseInt(t1.getText()), Integer.parseInt(t2.getText()), Integer.parseInt(t7.getText()), Integer.parseInt(t6.getText()), ta, ta1);
        Thread t = new Thread(m);
        t.start();
    }
}
