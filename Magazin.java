import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Thread.sleep;

public class Magazin implements Runnable {

    private int numarCase;
    private int timpServireMinim;
    private int timpServireMaxim;
    private int timpSosireMinim;
    private int timpSosireMaxim;
    private int timpSimulare;
    private int viteza;
    private int finalSimulare;
    private int timpGenerare;
    private int idClient;
    private int[] timpGol;
    private int[] peakTime;
    private int[] maxPeak;
    private int[] clientNo;
    private int[] serviceTime;
    private int[] waitingTime;
    private GeneratorClienti generatorClienti;
    private List<CasaMarcat> casaMarcatList;
    private JTextArea ta, ta1;

    public Magazin (int numarCase, int timpServireMinim, int timpServireMaxim, int timpSosireMinim, int timpSosireMaxim, int viteza, int finalSimulare, JTextArea ta, JTextArea ta1) {
        this.numarCase = numarCase;
        this.timpServireMinim = timpServireMinim;
        this.timpServireMaxim = timpServireMaxim;
        this.timpSosireMinim = timpSosireMinim;
        this.timpSosireMaxim = timpSosireMaxim;
        this.timpSimulare = 0;
        this.idClient=1;
        this.viteza = viteza;
        this.finalSimulare = finalSimulare;
        this.timpGenerare=0;
        casaMarcatList=new ArrayList<>();
        generatorClienti=new GeneratorClienti(this.timpSosireMinim, this.timpSosireMaxim, this.timpServireMinim, this.timpServireMaxim, this.viteza);
        for(int i=1; i<=numarCase; i++) {
            CasaMarcat casaMarcat = new CasaMarcat(i, viteza, ta);
            casaMarcatList.add(casaMarcat);
            Thread t = new Thread(casaMarcat);
            t.start();
        }
        timpGol=new int[numarCase+1];
        peakTime=new int[numarCase+1];
        maxPeak=new int[numarCase+1];
        clientNo=new int[numarCase+1];
        serviceTime=new int[numarCase+1];
        waitingTime=new int[numarCase+1];
        this.ta1=ta1;
        this.ta=ta;
    }


    @Override
    public void run() {
        ta.setText("");
        ta1.setText("");
        int timpSer=generatorClienti.timpServire();
        int timpSos=generatorClienti.timpSosire();
        Client client=new Client(idClient++, timpSer, timpSos);
        timpGenerare=timpSos;
        while(true) {
            ta1.setText("");
            for (CasaMarcat c : casaMarcatList) {
                System.out.println(c.getId()+" "+c.getList().size());
                ta1.append("Casa cu id:"+c.getId()+"\n");
                for(Client cl:c.getList())
                {
                    System.out.println(cl.toString());
                    ta1.append("Clientul cu id"+cl.getNumar()+"\n");
                }

            }
            try {
                sleep(viteza * 10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            timpSimulare++;

            ta.append("sec:"+timpSimulare+"\n");

            for (CasaMarcat c : casaMarcatList) {
                c.setTimpSimulare(timpSimulare);
                if(c.empty()==0)
                {
                    timpGol[c.getId()]++;
                }
                else {
                    c.setTimpAsteptare(c.getTimpAsteptare() - 1);
                }
            }
            Random rand = new Random();
            int r = rand.nextInt(numarCase)+1;
            int min=finalSimulare;
            for (CasaMarcat c : casaMarcatList) {
                {if (c.getTimpAsteptare() < min)
                min=c.getTimpAsteptare();}}
            if (timpSimulare == timpGenerare && (timpGenerare+ min)<finalSimulare) {


                for (CasaMarcat c : casaMarcatList) {
                    if (c.getId() == r)
                    {
                        c.addClient(client);
                        clientNo[c.getId()]++;
                        serviceTime[c.getId()]+=client.getServingTime();
                        waitingTime[c.getId()]+=c.getTimpAsteptare();
                        c.setTimpAsteptare(c.getTimpAsteptare()+client.getServingTime());
                        if(c.getTimpAsteptare()>maxPeak[c.getId()])
                        {
                            peakTime[c.getId()]=timpSimulare;
                            maxPeak[c.getId()]=c.getTimpAsteptare();
                        }
                    }
                }

                timpSer = generatorClienti.timpServire();
                timpSos = generatorClienti.timpSosire();
                timpGenerare += timpSos;
                client = new Client(idClient++, timpSer, timpSos);
                r = rand.nextInt(numarCase)+1;
            }
            synchronized (this) {
                if (timpSimulare == finalSimulare) {
                    try {
                        ta.append("\n");
                        ta.append("Timp case goale:\n");
                        for(int i=1;i<=numarCase;i++){
                            ta.append("Casa cu id: "+ i +" a avut un timp de in care a fost goala = "+timpGol[i]+"\n");
                        }
                        ta.append("\n");
                        ta.append("Peak time:\n");
                        for(int i=1;i<=numarCase;i++){
                            ta.append("Casa cu id: "+ i +" a avut ora de varf = "+peakTime[i]+" cu un timp de asteptare:"+ maxPeak[i]+"\n");
                        }
                        ta.append("\n");
                        ta.append("Average serving time per client:\n");
                        for(int i=1;i<=numarCase;i++){
                            ta.append("Casa cu id: "+ i +" a avut media de timp de servire = "+(float)serviceTime[i]/clientNo[i] +"\n");
                        }
                        ta.append("\n");
                        ta.append("Average waiting time per client:\n");
                        for(int i=1;i<=numarCase;i++){
                            ta.append("Timp de asteptare: "+waitingTime[i]+"\n");
                            ta.append("Numar de persoane: "+clientNo[i]+"\n");
                            ta.append("Casa cu id: "+ i +" a avut media de timp de asteptare = "+(float)waitingTime[i]/clientNo[i] +"\n");
                        }
                        ta.append("\n");
                        ta.append("Simularea s-a terminat!\n");
                        ta1.setText("");
                        for(CasaMarcat c:casaMarcatList){
                            ta1.append("Casa cu id:"+c.getId()+"\n");
                            ta1.append("Inchisa\n");
                        }
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }
}
